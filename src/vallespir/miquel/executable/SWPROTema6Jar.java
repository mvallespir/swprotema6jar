package vallespir.miquel.executable;

import vallespir.miquel.utilitats.Cronometre;
import vallespir.miquel.utilitats.OrdenacioICerca;
import static vallespir.miquel.utilitats.UtilitatsConsola.llegirSencer;
import static vallespir.miquel.utilitats.UtilitatsGenerals.nombreAleatori;

/**
 *
 * @author MVC
 */
public class SWPROTema6Jar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[] arrayEnters = new int[llegirSencer("Introdueix la longitud de l'array")];

        for (int i = 0; i < arrayEnters.length; i++) {
            arrayEnters[i] = nombreAleatori(100, 1);
        }

        SWPROTema6Jar tema6 = new SWPROTema6Jar();
        Cronometre crono = new Cronometre();
        OrdenacioICerca oIC = new OrdenacioICerca();

        tema6.mostrarArray(arrayEnters);
        crono.start();
        oIC.bimbolla(arrayEnters);
        crono.stop();

        System.out.println("\nLa ordenació ha tardat: " + crono.temps());
        System.out.println("");
        tema6.mostrarArray(arrayEnters);

    }

    public void mostrarArray(int[] mostrar) {
        for (int i : mostrar) {
            System.out.print(i + ", ");
        }
    }

}
